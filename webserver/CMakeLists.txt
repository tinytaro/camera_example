cmake_minimum_required(VERSION 3.12)

project(camserver)

add_compile_options(-Wall)

add_executable(server server.c)
target_link_libraries(server jpeg pthread)
